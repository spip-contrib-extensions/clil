<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'clil_description' => '<p>CLIL : Commission de Liaison Interprofessionnelle du Livre.</p>
	Ajoute dans le formulaire Livre du plugin Bouquinerie un sélecteur de thème CLIL. Ce thème CLIL répond à la norme ONIX 3.0 et permet d’améliorer le référencement de vos livres sur les différents sites de ventes.<br>

	- <b>Configuration</b> : Vous pouvez aller dans la page de gestion du plugin (Publication -> Thèmes CLIL) et sélectionner parmi les 1052 thèmes de la CLIL ceux spécifiques à vos besoins.',
	'clil_nom' => 'Thèmes CLIL',
	'clil_slogan' => 'Codifier une fiche Livre avec la classification des thèmes CLIL',
);

?>