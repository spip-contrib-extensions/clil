<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;
 
$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'clil_titre' => 'Thèmes CLIL',
	'cfg_exemple' => 'Exemple',
	'cfg_exemple_explication' => 'Explication de cet exemple',
	'cfg_titre_parametrages' => 'Choisir les thèmes CLIL correspondant à votre catalogue',

	// E
	'explication_interface_clil' => 'Cocher les thèmes CLIL qui seront affichés dans le sélecteur <i>Thème CLIL</i> de vos livres.',

	//O
	'onglet_clil' => 'Choisir thèmes CLIL',
	'onglet_clil_libelle' => 'Modifier libellés',
	'onglet_clil_rubrique' => 'Restreindre par rubriques',

	// T
	'titre_page_configurer_clil' => 'Thèmes CLIL',
);

?>