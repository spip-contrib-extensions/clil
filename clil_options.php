<?php
/**
 * Options du plugin Thèmes CLILau chargement
 *
 * @plugin     Thèmes CLIL
 * @copyright  2015
 * @author     Pierre Miquel
 * @licence    GNU/GPL
 * @package    SPIP\Clil\Options
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

// augmenter la profondeur d’exécution pour le plugin Extras chez hébergeurs ayant activés xdebug
// voir https://contrib.spip.net/Champs-Extras-3
ini_set('xdebug.max_nesting_level', 200);
