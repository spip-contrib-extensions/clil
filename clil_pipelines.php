<?php
/**
 * Utilisations de pipelines par Thèmes CLIL
 *
 * @plugin     Thèmes CLIL
 * @copyright  2015
 * @author     Pierre Miquel
 * @licence    GNU/GPL
 * @package    SPIP\Clil\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}